" Required
set nocompatible    " be iMproved, required
filetype off        " required

" Init plug
call plug#begin('~/.vim/plugins') " Installs plugins to folder

Plug 'itchyny/lightline.vim'            " Bloat free powerline
Plug 'ntpeters/vim-better-whitespace'   " Trailing space hightlight
Plug 'ryanoasis/vim-devicons'           " Various icons
Plug 'chr4/nginx.vim'                   " Nginx syntax
Plug 'editorconfig/editorconfig-vim'    " .editorconfig support
Plug 'machakann/vim-highlightedyank'    " Highlight yanked
Plug 'RRethy/vim-illuminate'            " Highlight the same strings
Plug 'ap/vim-css-color'                 " CSS color highlight
Plug 'vifm/vifm.vim'                    " Vifm
Plug 'alvan/vim-closetag'               " Autoclose (X)HTML tags

" End plugin block
call plug#end()
filetype plugin indent on

" Yank highlight duration
let g:highlightedyank_highlight_duration = 200

" Whitespace settings
let g:better_whitespace_ctermcolor='red'
let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1
let g:strip_whitespace_confirm=0

" Devicons
let g:webdevicons_enable = 1
let g:webdevicons_enable_nerdtree = 1
let g:webdevicons_conceal_nerdtree_brackets = 1

function! LightlineWebDevIcons(n)
    let l:bufnr = tabpagebuflist(a:n)[tabpagewinnr(a:n) - 1]
    return WebDevIconsGetFileTypeSymbol(bufname(l:bufnr))
endfunction

function! LightLineFilename()
    return WebDevIconsGetFileTypeSymbol(expand('%')) . ' ' . expand('%')
endfunction

" Lightline theme
let g:lightline = {
    \ 'colorscheme': 'one',
    \ 'active': {
    \   'right': [['lineinfo'], ['fileformat', 'filetype']]
    \ },
    \ 'component_function': {
    \   'filename': 'LightLineFilename'
    \ },
    \ 'component': {
    \   'lineinfo': "[%l:%-v] [%{printf('%03d/%03d',line('.'),line('$'))}]",
    \ },
    \ 'tab_component_function': {
    \   'tabnum': 'LightlineWebDevIcons',
    \ }
    \ }

" General settings
syntax enable                " Syntax highlighting
set title                    " Dynamic title name
set number relativenumber    " Relative number lines
set laststatus=2             " Status bar
set background=dark          " Sets the background mode to dark
set noshowmode               " Disable builtin mode display
set t_Co=256                 " 256 Color support
set encoding=UTF-8           " Set UTF-8 encoding
set mouse=nicr               " Mouse scrolling
set incsearch                " Incremental search
set hlsearch                 " Highlight all selected strings
set wildmenu                 " Enables wildmenu
" Broken on Debian since it uses an older vim version
set wildoptions=pum          " Vertical wildmenu (like nvim)

" Tabs and indenting
set autoindent               " Enable auto-indenting
set expandtab                " Use spaces instead of tabs.
set smarttab                 " Be smart using tabs ;)
set shiftwidth=4             " One tab == four spaces.
set tabstop=4                " One tab == four spaces.
