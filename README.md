# Yonei Arch Dotfiles

This is a collection of my configuration files for the tools i use   
Check my **[st](https://gitlab.com/y0nei/st)** / **[dmenu](https://gitlab.com/y0nei/dmenu)** builds since i use them as a replacement for alacritty and rofi now

### There are three avalible branches:
&boxhd; **[main](../../tree/main)** - *the main branch (uses [awesome-git](https://github.com/awesomeWM/awesome))*   
&boxvr; [awesome-4.3](../../tree/awesome-4.3) - *a branch for awesome 4.3 stable version* 🪦   
&boxur; [bspwm](../../tree/bspwm) - *a branch for older bspwm & polybar configs* 🪦

<sup>🪦 indicates unmaintained</sup>
## Table of contents

- [AwesomeWM config](.config/awesome/)
- Shell configs <sub><sup>*(some things taken from [DT's dotfiles](https://gitlab.com/dwt1/dotfiles/-/blob/master/.config/fish/config.fish))*</sup></sub>
	- [Zsh](.zshrc) / [Fish](.config/fish/config.fish) / [Starship](.config/starship.toml)
- [Nano config](.config/nano/nanorc) *(imitates vim so you can flex)*
- [Vim](.vimrc) and [Neovim](.config/nvim/) configs
- [Rofi config](.config/rofi)
- [Alacritty config](.config/alacritty.yml)
- [Picom](.config/picom.conf) *(using [ibhagwan's fork](https://github.com/ibhagwan/picom))*   
*... and more*
