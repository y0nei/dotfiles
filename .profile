[[ -f ~/.bashrc ]] && . ~/.bashrc

# Exports
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}

# ~/ cleanup
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export GOPATH="$XDG_DATA_HOME/go"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export HISTFILE="$XDG_DATA_HOME/bash-history"
export LESSHISTFILE=-

# Default apps
export TERMINAL="st"
export TERM="xterm-256color"
export BROWSER="librewolf"
export EDITOR="nvim"
export SUDO_EDITOR="nvim"
export VISUAL="codium"

if [[ "$(tty)" = "/dev/tty1" ]]; then
	pgrep awesome || startx "$HOME/.xinitrc"
fi
