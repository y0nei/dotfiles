local wibox = require("wibox")
local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local lain = require("lain")
local markup = lain.util.markup

-- Create a textclock widget
local textclock = wibox.widget {
    {
        format = "%a, %d %b %H:%M",
        widget = wibox.widget.textclock
    },
    left = 5,
    right = 5,
    widget = wibox.container.margin
}

-- Systray
local systray = wibox.widget.systray()
systray:set_base_size(dpi(25))

-- Linear gradient for progress bar
local gradient = gears.color.create_linear_pattern({
    type = "linear",
    from = { 25, 0 }, to = { 100, 0 },
    stops = { { 0, beautiful.bg_highlight }, { 100, "#d79921" } }
})

-- {{ Ram usage bar
-- Progress bar
local mem_progress_bar = wibox.widget {
    max_value        = 100,
    forced_width     = 100,
    shape            = function(cr,w,h) gears.shape.rounded_rect(cr,w,h,beautiful.border_radius) end,
    color            = gradient,
    --color            = beautiful.bg_highlight,
    background_color = beautiful.bg_minimize,
    widget           = wibox.widget.progressbar,
}

-- Lain mem widget
local mem = lain.widget.mem({
    timeout = 4,
    settings = function()                                    -- Round to two decimal places and convert MiB to GiB
        widget:set_markup(markup.font("Rubik 11", "RAM: " .. string.format("%.2f", mem_now.used / 1024) .. " GiB"))
        mem_progress_bar.value = mem_now.perc
    end
})

-- Mem bar widget
local mem_bar = wibox.widget {
    {
        {
            mem_progress_bar,
            widget = wibox.container.background
        },
        {
            -- Margins and widget
            {
                mem,
                fg = "#ffffff",
                widget = wibox.container.background
            },
            left = 7,
            right = 7,
            widget = wibox.container.margin
        },
        layout = wibox.layout.stack
    },
    margins = 3,
    widget = wibox.container.margin
}
-- }}

-- {{ Cpu progress bar
local cpu_progress_bar = wibox.widget {
    max_value        = 100,
    width            = 50,
    shape            = function(cr,w,h) gears.shape.rounded_rect(cr,w,h,beautiful.border_radius) end,
    color            = gradient,
    --color            = beautiful.bg_highlight,
    background_color = beautiful.bg_minimize,
    widget           = wibox.widget.progressbar,
}

-- Cpu temp widget
local temp = wibox.widget {
    widget = wibox.widget.textbox
}

-- Lain cpu and temp widget
local temp_cmd = [[cat /sys/class/hwmon/hwmon1/temp2_input]]
local cpu = lain.widget.cpu({
    timeout = 4,
    settings = function()
        widget:set_markup(markup.font("Rubik 11", "CPU: " .. cpu_now.usage .. "% | " ))
        cpu_progress_bar.value = cpu_now.usage
        -- async read temp and update temp widget text
        awful.spawn.easy_async(temp_cmd, function(stdout)
            temp.text = string.format("%.0f°C", stdout / 1000 )
        end)
    end
})

local cpu_bar = wibox.widget {
    {
        {
            -- background progressbar
            cpu_progress_bar,
            widget = wibox.container.background
        },
        {
            {
                -- cpu and temp widgets
                {
                    cpu,
                    fg = "#ffffff",
                    widget = wibox.container.background
                },
                {
                    temp,
                    fg = "#ffffff",
                    widget = wibox.container.background
                },
                layout = wibox.layout.align.horizontal
            },
            left = 7,
            right = 7,
            widget = wibox.container.margin
        },
        layout = wibox.layout.stack
    },
    margins = 3,
    widget = wibox.container.margin
}
-- }}


-- {{ Cpu progress bar
local battery_progress_bar = wibox.widget {
    max_value        = 100,
    width            = 50,
    shape            = function(cr,w,h) gears.shape.rounded_rect(cr,w,h,beautiful.border_radius) end,
    color            = gradient,
    --color            = beautiful.bg_highlight,
    background_color = beautiful.bg_minimize,
    widget           = wibox.widget.progressbar,
}

-- Battery drainage widget
local batt_drainage = wibox.widget {
    widget = wibox.widget.textbox
}

-- Lain cpu and temp widget
local batt_drainage_cmd = [[cat /sys/class/power_supply/BAT0/power_now]]
local battery = lain.widget.bat({
    timeout = 4,
    settings = function()
        widget:set_markup(markup.font("Rubik 11", "BAT: " .. bat_now.perc .. "% | " ))
        battery_progress_bar.value = bat_now.usage
        -- async read temp and update temp widget text
        awful.spawn.easy_async(batt_drainage_cmd, function(stdout)
            batt_drainage.text = string.format("%W", stdout * 10^-6 )
        end)
    end
})

local battery_bar = wibox.widget {
    {
        {
            -- background progressbar
            battery_progress_bar,
            widget = wibox.container.background
        },
        {
            {
                -- battery and drainage widgets
                {
                    battery,
                    fg = "#ffffff",
                    widget = wibox.container.background
                },
                {
                    batt_drainage,
                    fg = "#ffffff",
                    widget = wibox.container.background
                },
                layout = wibox.layout.align.horizontal
            },
            left = 7,
            right = 7,
            widget = wibox.container.margin
        },
        layout = wibox.layout.stack
    },
    margins = 3,
    widget = wibox.container.margin
}
-- }}

screen.connect_signal("request::desktop_decoration", function(s)
    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()

    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox {
        screen  = s,
        buttons = {
            awful.button({ }, 1, function () awful.layout.inc( 1) end),
            awful.button({ }, 3, function () awful.layout.inc(-1) end),
            awful.button({ }, 4, function () awful.layout.inc(-1) end),
            awful.button({ }, 5, function () awful.layout.inc( 1) end),
        }
    }

    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        style   = {
            shape = function(cr,w,h)
                gears.shape.rounded_rect(cr,w,h,beautiful.border_radius)
            end
        },
        widget_template = {
            {
                {
                    {
                        id     = "text_role",
                        widget = wibox.widget.textbox,
                    },
                    layout = wibox.layout.fixed.horizontal,
                },
                left  = 10,
                right = 10,
                widget = wibox.container.margin
            },
            id     = "background_role",
            widget = wibox.container.background,
        },
        buttons = {
            awful.button({ }, 1, function(t) t:view_only() end),
            awful.button({ modkey }, 1, function(t)
                                            if client.focus then
                                                client.focus:move_to_tag(t)
                                            end
                                        end),
            awful.button({ }, 3, awful.tag.viewtoggle),
            awful.button({ modkey }, 3, function(t)
                                            if client.focus then
                                                client.focus:toggle_tag(t)
                                            end
                                        end),
            awful.button({ }, 4, function(t) awful.tag.viewprev(t.screen) end),
            awful.button({ }, 5, function(t) awful.tag.viewnext(t.screen) end),
        }
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        style   = {
            shape = function(cr,w,h)
                gears.shape.rounded_rect(cr,w,h,beautiful.border_radius)
            end
        },
        layout  = {
            spacing = 10,
            layout  = wibox.layout.flex.horizontal
        },
        buttons = {
            awful.button({ }, 1, function (c)
                c:activate { context = "tasklist", action = "toggle_minimization" }
            end),
            awful.button({ }, 3, function() awful.menu.client_list { theme = { width = 250 } } end),
            awful.button({ }, 4, function() awful.client.focus.byidx(-1) end),
            awful.button({ }, 5, function() awful.client.focus.byidx( 1) end),
        }
    }

    -- Create the wibox
    s.mywibox = awful.wibar {
        position = "top",
        screen   = s,
        height = dpi(25),
        widget   = {
            layout = wibox.layout.align.horizontal,
            { -- Left widgets
                layout = wibox.layout.fixed.horizontal,
                mylauncher,
                s.mytaglist,
                s.mypromptbox,
            },
            s.mytasklist, -- Middle widget
            { -- Right widgets
                layout = wibox.layout.fixed.horizontal,
                systray,
                cpu_bar,
                mem_bar,
                battery_bar,
                textclock,
                s.mylayoutbox,
            },
        }
    }
end)
