-- awesome_mode: api-level=4:screen=on
-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

--[[ Error handling ]]--
naughty.connect_signal("request::display_error", function(message, startup)
    naughty.notification {
        urgency = "critical",
        title = "Oops, an error happened"..(startup and " during startup!" or "!"),
        message = message
    }
end)

--[[ Theme ]]--
beautiful.init(gears.filesystem.get_configuration_dir() .. "theme.lua")

--[[ Defaults ]]--
_G.modkey = "Mod4"
_G.terminal = "st"
_G.editor = os.getenv("EDITOR") or "vim"
_G.editor_cmd = terminal .. " -e " .. editor

--[[ Menubar ]]--
require("config.menu")

--[[ Tag layout ]]--
require("config.tags")

--[[ Wallpaper ]]--
screen.connect_signal("request::wallpaper", function(s)
    awful.wallpaper {
        screen = s,
        widget = {
            {
                image     = beautiful.wallpaper,
                upscale   = true,
                downscale = true,
                widget    = wibox.widget.imagebox,
            },
            valign = "center",
            halign = "center",
            tiled  = false,
            widget = wibox.container.tile,
        }
    }
end)

--[[ Wibar ]]--
require("decoration.wibar")

--[[ Key & Mouse bindings ]]--
require("config.binds")

--[[ Rules ]]
require("config.rules")

--[[ Titlebars ]]--
-- require("decoration.titlebars")

-- Notifications
naughty.connect_signal("request::display", function(n)
    naughty.layout.box { notification = n }
end)

-- Enable sloppy focus, so that focus follows mouse.
--[[
client.connect_signal("mouse::enter", function(c)
    c:activate { context = "mouse_enter", raise = false }
end)
--]]
