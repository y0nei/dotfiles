require("user.options")
require("user.plugins")
require("user.bootstrap")
require("user.keymaps")
require("user.nvim-tree")
require("user.cmp")
require("user.lsp")
require("user.null-ls")
require("user.treesitter")
require("user.lualine")

--[[ Colors ]]--
-- require('user.base16-colors')
require("user.onedark")

vim.api.nvim_set_hl(0, "CursorLineNr", { fg = "#F0C674" }) -- cursor line number color

--[[ Plugin setups ]]--
require("presence"):setup()
require('Comment').setup()
require("nvim-autopairs").setup()
require('nvim-ts-autotag').setup({
    filetypes = { "html", "htmldjango", "xml" },
})
require("colorizer").setup({
    filetypes = {
        '*';
        css = { css = true; tailwind = true; };
        html = { css = true; tailwind = true; };
        htmldjango = { css = true; tailwind = true; }
    },
})
require("indent_blankline").setup({
    char = "▏",
    -- char = "┊",
    -- TODO: Look issue 522
    show_trailing_blankline_indent = false,
    show_current_context = true,
    -- show_current_context_start = true,
    -- use_treesitter = true,
})
require("bufferline").setup({
    options = {
        show_buffer_close_icons = false,
        separator_style = "thick",
        offsets = {
            {
                filetype = "NvimTree"
            }
        }
    }
})
require("gitsigns").setup({
    -- Other cool chars: 🮙, 🮕, ╎, ╏, ┆, ┇, ┊, ┋
    signs = {
        add          = { hl = 'GitSignsAdd', text = '▎', numhl = 'GitSignsAddNr', linehl = 'GitSignsAddLn' },
        change       = { hl = 'GitSignsChange', text = '▎', numhl = 'GitSignsChangeNr', linehl = 'GitSignsChangeLn' },
        delete       = { hl = 'GitSignsDelete', text = '_', numhl = 'GitSignsDeleteNr', linehl = 'GitSignsDeleteLn' },
        topdelete    = { hl = 'GitSignsDelete', text = '‾', numhl = 'GitSignsDeleteNr', linehl = 'GitSignsDeleteLn' },
        changedelete = { hl = 'GitSignsChange', text = '~', numhl = 'GitSignsChangeNr', linehl = 'GitSignsChangeLn' },
        untracked    = { hl = 'LineNr', text = '┆', numhl = 'GitSignsAddNr', linehl = 'GitSignsAddLn' },
    }
})
