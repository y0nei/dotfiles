local ok, packer = pcall(require, "packer")
if not ok then
    print("Packer is not installed")
    return
end

-- Have packer use a popup window
packer.init {
    display = {
        open_fn = function()
            return require("packer.util").float { border = "rounded" }
        end
    }
}

-- PackerSync on file save
vim.cmd([[
    augroup packer_user_config
        autocmd!
        autocmd BufWritePost plugins.lua source <afile> | PackerSync
    augroup end
]])

packer.startup(function(use)
    use "wbthomason/packer.nvim"

    use "glepnir/dashboard-nvim" -- Dashboard
    use "nvim-lualine/lualine.nvim" -- better statusline
    use "lewis6991/gitsigns.nvim" -- git sign column
    use "akinsho/bufferline.nvim" -- bufferline
    use "lukas-reineke/indent-blankline.nvim" -- show line indentations
    use "numToStr/Comment.nvim" -- better commenting
    use "nvim-lua/plenary.nvim"
    use "andweeb/presence.nvim" -- discord presence

    -- colorschemes
    use "RRethy/nvim-base16" -- Base16 theme collection
    use "navarasu/onedark.nvim" -- onedark theme

    use { "nvim-tree/nvim-tree.lua", -- file browser + devicons
        requires = { "nvim-tree/nvim-web-devicons" },
        tag = "nightly" -- optional, updated every week.
    }

    -- treesitter
    use { "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" }
    use "nvim-treesitter/playground" -- view treesitter information

    use "windwp/nvim-autopairs" -- bracket closing
    use "p00f/nvim-ts-rainbow" -- bracket colorizer (unmaintained since 2023)
    use "windwp/nvim-ts-autotag" -- autoclose html tags
    use "RRethy/vim-illuminate" -- highlighting support
    use "NvChad/nvim-colorizer.lua" -- color highlights

    -- LSP
    use {
        "williamboman/mason.nvim", -- language server installer
        "williamboman/mason-lspconfig.nvim", -- mason language server bridge
        "neovim/nvim-lspconfig", -- builtin neovim LSP
    }
    use "jose-elias-alvarez/null-ls.nvim" -- LSP diagnostics and code actions

    -- autocompletion
    use "hrsh7th/nvim-cmp" -- completion plugin
    use {
        "L3MON4D3/LuaSnip", -- snippet engine
        "saadparwaiz1/cmp_luasnip", -- completion source for cmp
    }

    use "hrsh7th/cmp-nvim-lsp" -- lsp support for cmp
    use "hrsh7th/cmp-buffer" -- buffer completions
    use "hrsh7th/cmp-path" -- path completions
    use "hrsh7th/cmp-nvim-lua" -- neovim lua api
    use "hrsh7th/cmp-nvim-lsp-signature-help" -- signature help
    use({ "roobert/tailwindcss-colorizer-cmp.nvim", -- tailwind cmp colorizer
        -- optionally, override the default options:
        config = function()
            require("tailwindcss-colorizer-cmp").setup({
                color_square_width = 1,
            })
        end
    })
    use "chrisgrieser/cmp-nerdfont" -- nerd font completions
    use "rafamadriz/friendly-snippets" -- a bunch of snippets to use
end)
