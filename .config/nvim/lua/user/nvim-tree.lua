require("nvim-tree").setup {
    update_focused_file = {
        enable = true,
        update_cwd = true
    },
    renderer = {
        highlight_opened_files = "name",
        indent_markers = {
            enable = true
        },
        icons = {
            glyphs = {
                git = {
                    unstaged = "",
                    staged = "S",
                    unmerged = "",
                    renamed = "➜",
                    untracked = "U",
                    deleted = "",
                    ignored = "◌",
                }
            }
        }
    },
    diagnostics = {
        enable = true,
        show_on_dirs = true
    },
    git = { ignore = false }
}
