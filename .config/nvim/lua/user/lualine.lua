require('lualine').setup {
    options = {
        theme = 'onedark',
        --theme  = require('lualine.themes.onedark'),
        --section_separators = '',
        disabled_filetypes = { "dashboard", "NvimTree" },
    },
    sections = {
        lualine_b = {
            { "branch", icon = "" },
            { "diagnostics",
                sources = { "nvim_diagnostic" },
                sections = { "error", "warn" },
                symbols = { error = " ", warn = " " },
                colored = true,
                update_in_insert = false,
                always_visible = false,
            }
        }
    }
}
