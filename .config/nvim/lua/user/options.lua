local options = {
    number = true,             -- numberline
    numberwidth = 2,           -- number column width (default: 4)
    relativenumber = true,     -- relative number line
    cursorline = true,         -- cursor line
    title = true,              -- enable window title
    showmode = false,          -- dont show builtin mode display
    pumheight = 10,            -- limit pum height
    mouse = "a",               -- allow mouse interaction in all modes
    clipboard = "unnamedplus", -- access the system clipboard
    history = 100,             -- Number of commands to remember in a history table
    termguicolors = true,      -- enable 24-bit colors in TUI
    splitright = true,         -- vsplit to right
    list = true,               -- whitespace highlighting
    fileencoding = "utf-8",    -- default file encoding
    ignorecase = true,         -- case insensitive search
    smartcase = true,          -- case insensitive search
    showtabline = 2,           -- always show tabs
    wrap = false,              -- dont wrap long lines
    scrolloff = 8,             -- min. number of lines kept above and below of the cursor
    sidescrolloff = 8,         -- min. number of lines kept left and right of the cursor
    timeoutlen = 400,          -- time to wait for a mapped sequence to complete (in milliseconds)
    swapfile = false,          -- creates a swapfile
    undofile = true,           -- enable persistent undo
    writebackup = false,       -- disable making a backup before overwriting a file
    updatetime = 300,          -- faster completion (4000ms default)

    -- Intentation
    expandtab = true,          -- tabs to spaces
    tabstop = 4,               -- number of tab spaces
    shiftwidth = 4,            -- number of spaces per each indentation
    softtabstop = -1,          -- if negative, shiftwidth value is used
    copyindent = true,         -- copy the previous indentation on autoindenting
    smartindent = true,        -- make indentation smarter
    cindent = true,            -- enable indenting in c
}

--[[ Other settings ]]--
-- vim.opt.whichwrap = "bs<>[]hl"    -- wrap cursor when going over the line
vim.opt.iskeyword:append "-"      -- hyphenated words recognized by searches
vim.opt.fillchars = { eob = " " } -- disable `~` on nonexistent lines

vim.opt.formatoptions:remove({ "c", "r", "o" })
vim.opt.completeopt = { "menuone", "noselect" } -- options for insert mode completion

-- [[ Highlight on yank ]]
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})

for k, v in pairs(options) do
    vim.opt[k] = v
end
